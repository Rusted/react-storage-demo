import React from 'react';

const Index = (props) => {
  const categories = props.categories.map((category, i) => {
    return (
      <li key={i}>{category}</li>
    );
  });

  return <ul>{categories}</ul>;
};

export default Index;