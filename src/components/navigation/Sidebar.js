import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import NavItem from './NavItem';
import _ from 'lodash';

const Sidebar = (props) => {
  const categories = props.categories.map((category)=>{
    return (
      <NavItem to={'/items/' + category} exact={true} key={_.uniqueId()}>{category}</NavItem>
    );
  });

  categories.push (<NavItem to={'/items/all'} exact={true} key={_.uniqueId()}>all</NavItem>);

  return (
    <div className="sidebar">
      <h2>categories</h2>
      <ul>
        {categories}
      </ul>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    categories: state.category.categories,
  };
};

export default withRouter(connect(mapStateToProps)(Sidebar));