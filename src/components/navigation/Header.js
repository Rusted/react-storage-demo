import React from 'react';
import NavItem from './NavItem';

const Header = () => {
  return (
    <div className="header">
      <ul>
        <NavItem to="/items">Items</NavItem>
        <NavItem to="/add-items">Add items</NavItem>
        <NavItem to="/categories">Categories</NavItem>
        <NavItem to="/statistics">Statistics</NavItem>
      </ul>
    </div>
  );
};

export default Header;