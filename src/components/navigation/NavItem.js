import React from 'react';
import { NavLink } from 'react-router-dom';

const navigationItem = ( props ) => (
  <li className="navigationLink">
    <NavLink 
      to={props.to}
      exact={props.exact}
      activeClassName="active">{props.children}</NavLink>
  </li>
);

export default navigationItem;