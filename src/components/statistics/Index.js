import React from 'react';
import Aux from '../../hoc/Aux';

const Index = (props) => {
  const categoryCounts = Object.keys(props.counts).map((categoryName)=>{
    return (
      <li key={categoryName}>{categoryName}:{props.counts[categoryName]}</li>
    );
  });
      
  return <Aux>
    <h3>Total items: {props.total}</h3>
    <h3>Items per category:</h3>
    <ul>
      {categoryCounts}
    </ul>
    <h3>Total sum: {props.sum.toFixed(2)}</h3>
  </Aux>;
};

export default Index;