import React from 'react';

const Row = (props) => {
  return <div className="item">
    <h4>{props.title}</h4>
    <p>{props.type}</p> 
    <p>{props.price}€</p>
    <p><button className="btn" onClick={()=>props.onRemoveItem(props.id)}>delete</button></p>
  </div>;
};

export default Row;