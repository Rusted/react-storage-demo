import React from 'react';
import Row from './Row';
import Aux from '../../hoc/Aux';

const Index = (props) => {
  let items = props.items.map((item) => 
    <Row 
      id={item.id}
      title={item.title}
      type={item.type}
      price={item.price}
      key={item.id}
      onRemoveItem={item.onRemoveItem}
    ></Row>
  );

  if (items.length == 0) {
    items = <div className="notFound">No items found.</div>;
  }
  
  return <Aux>{items}</Aux>;
};

export default Index;