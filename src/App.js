import React, { Component } from 'react';
import './App.scss';
import Header from './components/navigation/Header';
import Items from './containers/Items';
import AddItems from './containers/AddItems';
import Categories from './containers/Categories';
import Statistics from './containers/Statistics';
import Sidebar from './components/navigation/Sidebar';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';

class App extends Component {
  getRoutes = () => {
    return (
      <Switch>
        <Route path="/items/:filter" component={Items}/>
        <Route path="/add-items" component={AddItems} />
        <Route path="/categories" component={Categories} />
        <Route path="/statistics" component={Statistics} />
        <Redirect to="/items/all" />
      </Switch>
    );
  }

  render() {
    return (
      <div className="App">
        <Header/>
        <div className="row">
          <Sidebar/>
          {this.getRoutes()}
        </div>
      </div>
    );
  }
}

export default withRouter(App);