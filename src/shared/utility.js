export const checkValidity = ( value, rules ) => {
  if ( !rules ) {
    return;
  }

  if ( rules.required ) {
    if (value.trim() === '') {
      return 'this field is required!';
    }
  }

  if ( rules.price ) {
    if (!(/^\s*-?\d+(\.\d{1,2})?\s*$/).test( value )) {
      return 'invalid price!';
    }
  }

  if ( rules.alphaNumeric ) {
    if (!/^[a-z\d\-_]+$/i.test( value )) {
      return 'Alphanumeric characters only!';
    }
  }

  if ( rules.checkDuplicate ) {
    if (rules.checkDuplicate.indexOf(value) > -1) {
      return 'Duplicate value!';
    }
  }
};