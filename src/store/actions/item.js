import * as actionTypes from './actionTypes';

export const addItem = ( item ) => {
  return {
    type: actionTypes.ADD_ITEM,
    item: item
  };
};

export const removeItem = ( id ) => {
  return {
    type: actionTypes.REMOVE_ITEM,
    id: id
  };
};