import * as actionTypes from './actionTypes';

export const addCategory = ( categoryName ) => {
  return {
    type: actionTypes.ADD_CATEGORY,
    categoryName: categoryName
  };
};