import * as actionTypes from '../actions/actionTypes';
import _ from 'lodash';

const initialState = {
  items: [
    {title: 'dell', type: 'laptops', price: 550, id:_.uniqueId()},
    {title: 'lenovo', type: 'laptops', price: 550, id:_.uniqueId()},
    {title: 'dell', type: 'monitors', price: 100, id:_.uniqueId()},
    {title: 'apple', type: 'laptops', price: 1500, id:_.uniqueId()},
    {title: 'acme', type: 'keyboards', price: 15, id:_.uniqueId()}
  ],
};

const addItem = ( state, action ) => {
  let newItems = [...state.items];
  newItems.push(action.item);

  return {
    items: newItems
  };
};

const removeItem = ( state, action ) => {  
  const newItems = state.items.filter(item => item.id != action.id);
  return {
    items: newItems
  };
};
  
const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {
  case actionTypes.ADD_ITEM: return addItem( state, action );
  case actionTypes.REMOVE_ITEM: return removeItem( state, action );
  default: return state;
  }
};

export default reducer;