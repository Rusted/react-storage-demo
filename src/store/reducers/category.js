import * as actionTypes from '../actions/actionTypes';

const initialState = {
  categories: ['keyboards', 'laptops', 'monitors', 'hard-drives']
};

const addCategory = ( state, action ) => {
  let newCategories = [...state.categories];
  newCategories.push(action.categoryName);

  return {
    categories: newCategories
  };
};

const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {
  case actionTypes.ADD_CATEGORY: return addCategory( state, action );
  default: return state;
  }
};

export default reducer;