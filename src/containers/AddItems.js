import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import _ from 'lodash';
import {checkValidity } from '../shared/utility';

class AddItems extends React.Component {

  state = {
    showSuccess: false,
    titleError: true,
    title: '',
    price: '',
    type: this.props.categories[0],
    priceError: true,
    touched: false
  }

  checkErrors = (callback) => {
    this.setState({
      touched: true,
      titleError: checkValidity(this.state.title, {required: true}),
      priceError: checkValidity(this.state.price, {price: true}),
      showSuccess: false
    }, callback);
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.checkErrors(function() {
      if (!this.state.titleError && !this.state.priceError) {
        const newItem = {
          title: this.state.title,
          type: this.state.type,
          price: this.state.price,
          id: _.uniqueId()
        };
        
        this.props.onItemAdded(newItem);
        this.setState({
          showSuccess: true
        });
      }
    });
  }

  render() {
    const cats = this.props.categories.map((cat, i)=>{
      return (
        <option key={i} value={cat}>{cat}</option>
      );
    });

    return (
      <div className="add-items">
        {
          this.state.showSuccess ? <p className="success">Item added successfully!</p> : null
        }
        <h3>Add item to store</h3>
        <form 
          action="" 
          onSubmit={this.onSubmit}
        >
          <input 
            onChange={(event) => this.setState({title: event.target.value}, () => this.checkErrors(null))}
            className={this.state.touched && this.state.titleError ? 'danger' : ''}
            type="text" placeholder="title"
          />
          {this.state.titleError && this.state.touched ? (<label className="danger">{this.state.titleError}</label>) : ''}
          <select onChange={(event) => this.setState({type: event.target.value}, () => this.checkErrors(null))}>
            {cats}
          </select>
          <input
            onChange={(event) => this.setState({price: event.target.value}, () => this.checkErrors(null))}
            className={this.state.touched && this.state.priceError ? 'danger' : ''}
            type="text" placeholder="price"
          />
          {this.state.priceError && this.state.touched ? (<label className="danger">{this.state.priceError}</label>) : ''}
          <input className="btn" type="submit" value="add"/>
        </form>
      </div>
    );
  }
  
}

const mapStateToProps = state => {
  return {
    items: state.item.items,
    categories: state.category.categories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onItemAdded: (item) => dispatch(actions.addItem(item))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddItems);