import React from 'react';
import { connect } from 'react-redux';
import StatisticsIndex from '../components/statistics/Index';

class Statistics extends React.Component {

  render() {
    const items = [...this.props.items];

    const total = items.length;
    let sum = 0;
    let counts = {};
    items.forEach((i)=>{
      sum += parseFloat(i.price);
      if (!counts.hasOwnProperty(i.type)) {
        counts[i.type] = 1;
      } else {
        counts[i.type]++;
      }
    });

    return (
      <div className="statistics">
        <StatisticsIndex counts={counts} total={total} sum={sum} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.item.items,
    categories: state.category.categories,
  };
};

export default connect(mapStateToProps)(Statistics);