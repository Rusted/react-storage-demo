import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import CategoryIndex from '../components/category/Index';
import {checkValidity } from '../shared/utility';

class Categories extends React.Component {

  state = {
    showSuccess: false,
    titleError: true,
    title: '',
    touched: false
  }

  checkErrors = (callback) => {
    this.setState({
      touched: true,
      titleError: checkValidity(this.state.title, {required: true, alphaNumeric: true, checkDuplicate: this.props.categories}),
    }, callback);
  }

  onSubmit = (event) => {
    event.preventDefault();
    this.checkErrors(function() {
      if (!this.state.titleError) {
        this.props.onCategoryAdded(this.state.title);
        this.setState({
          showSuccess: true
        });
      }
    });
  }

  render() {
    return (
      <div className="categories">
        <h3>Add category</h3>
        <form action="" onSubmit={this.onSubmit}>
          <input
            onChange={(event) => this.setState({title: event.target.value}, () => this.checkErrors(null))}
            placeholder="title"
            type="text"
          />
          {this.state.titleError && this.state.touched ? (<label className="danger">{this.state.titleError}</label>) : ''}
          <input className="btn" type="submit" value="add"/>
        </form>
        <div>
          <h3>Existing categories</h3>
          <CategoryIndex categories={this.props.categories} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    categories: state.category.categories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onCategoryAdded: (categoryName) => dispatch(actions.addCategory(categoryName))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);