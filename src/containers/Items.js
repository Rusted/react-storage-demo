import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../store/actions/index';
import { withRouter } from 'react-router-dom';
import ItemIndex from '../components/item/Index';
 

const Items = (props) => {
  let filter = props.match.params.filter;
  if (filter == null) {
    filter = 'all';
  }

  let items = props.items
    .filter((item)=> {
      if (filter === 'all') {
        return true;
      }

      return filter === item.type;
    });

  return (
    <div className="items">
      <h1>{filter}</h1>
      <ItemIndex items={items} />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    items: state.item.items,
    categories: state.category.categories,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    removeItem: (categoryName) => dispatch(actions.removeItem(categoryName))
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Items));