# Description #
Demo Single page React.js application - Daumantas Urbanavičius
# How to run #
1) Run "npm install" in the project folder
2) Run "npm start" to view the project
# Features #
1) React 16
2) Javascript version is "ECMAScript 2015" or "ES6"
3) Separation of container and pure components, use of auxiliary component
4) Custom validation on input change - minimal demonstration
5) React router - minimal demonstration
6) Redux reducers - minimal demonstration